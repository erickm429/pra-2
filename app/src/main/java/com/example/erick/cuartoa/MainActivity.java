package com.example.erick.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button botonlogin, botoningresar, botonbuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonlogin = (Button) findViewById(R.id.btnlogin);
        botoningresar = (Button) findViewById(R.id.btnguardar);
        botonbuscar = (Button) findViewById(R.id.btnbuscar);

        botonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActividadLogin.class);
                startActivity(intent);
            }
        });
    }
}
